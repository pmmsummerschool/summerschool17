/*
 * Copyright University of Basel, Graphics and Vision Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package evaluation

import java.io._
import java.net.{InetAddress, Socket}
import javax.swing.JFileChooser

import scalismo.faces.io.{MoMoIO, RenderParameterIO}
import scalismo.faces.parameters.RenderParameter
import scalismo.mesh.TriangleMesh3D

/**
  * Simple evaluation client for the pmm summerschool '17. There are
  * several options how to submit a result.
  */
object EvaluationClient {
  scalismo.initialize()

  // location of your result file if you choose sendFromDisk()
  val rpsFileLocation: String = s"results/fitted-target.rps"

  // location of your model
  val modelFileLocation: String = s"data/model.h5"

  // name of your group
  val _GROUPNAME: String = "default"

  // port of the evaluation server listens to
  val _PORT = 8086

  // address of the evaluation server
  val _SERVER = "localhost"

  def main(args: Array[String]) {
//    val parameters = chooseFileToSend()
    val parameters = loadFromDisk()
    val momoFull = MoMoIO.read(new File(modelFileLocation)).get
    val momo = momoFull.neutralModel.truncate(parameters.momo.shape.size,parameters.momo.color.size)
    val mesh = momo.instance(parameters.momo.coefficients).shape
    sendMesh(mesh)
  }

  def sendMesh(mesh: TriangleMesh3D): Unit = {
    submit(mesh,_GROUPNAME,_SERVER,_PORT)
  }


  // chose rps file using an UI then submit the result
  def chooseFileToSend(): RenderParameter = {
    val jFileChooser = new JFileChooser(new File("data/"))
    if (jFileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
      val resultFile = jFileChooser.getSelectedFile
      val result = RenderParameterIO.read(resultFile).get
      result
    } else {
      throw new Exception("No parameter file select.... Quit submission process now.")
    }
  }

  // send the preconfigured file to the evaluation server
  def loadFromDisk(): RenderParameter = {
    val resultFile = new File(rpsFileLocation)
    val result = RenderParameterIO.read(resultFile).get
    result
  }

  // submits a render parameter to the evaluation server
  def submit(mesh: TriangleMesh3D, groupName: String, server: String, port: Int): Unit = {
    try {
      val ia = InetAddress.getByName(server)
      val socket = new Socket(ia, port)
      val out = new ObjectOutputStream(new DataOutputStream(socket.getOutputStream))
      val in = new ObjectInputStream(new DataInputStream(socket.getInputStream))
      out.writeObject((groupName, mesh.pointSet.points.toIndexedSeq.flatMap(pt => pt.toArray)))
      out.flush()
      val value = in.readDouble()
      println(s"You got the score: $value")
      in.close()
      out.close()
      socket.close()
    }
    catch {
      case e: IOException =>
        println(s"Error during evaluation using the server ${_SERVER}:${_PORT}")
        e.printStackTrace()
    }
  }

}
