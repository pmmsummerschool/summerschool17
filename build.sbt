name := "summerschool17"

version := "0.1"

scalaVersion := "2.11.8"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

resolvers += Resolver.jcenterRepo

resolvers += Resolver.bintrayRepo("unibas-gravis", "maven")

resolvers += "Statismo (public)" at "https://statismo.cs.unibas.ch/repository/public/"

libraryDependencies += "ch.unibas.cs.gravis" %% "scalismo-faces" % "0.5.0-RC3"

libraryDependencies += "org.scalanlp" %% "breeze-viz" % "0.13"

/* Uncomment the next line when you want to use the scalismo-ui for visualization. */
//libraryDependencies += "ch.unibas.cs.gravis" %% "scalismo-ui" % "0.11.+"

