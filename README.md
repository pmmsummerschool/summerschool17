# Summerschool ´17 on Probabilistic Morphable Models

This is the project repository for the practial exercises of the summerschool from the University of Basel on Probabilistic Morphable Models. The summerschool builds up on the course [Statistical Shape modeling](http://shapemodelling.cs.unibas.ch/StatisticalShapeModelling.html). The practical exercises are only an introduction into the software and how the concepts from the accompanying lectures translate to our framework.

## Getting started

The first thing you have to do is to get a copy of this repository. Either you can use git to checkout this repository (recommended) or you download it manualy using the browser.

When you have a copy of the repository go to the `doc` folder. There you can find the exercises which start with `day1/index.html`. The first day starts with the installation of all nescessary tools before you start coding.
